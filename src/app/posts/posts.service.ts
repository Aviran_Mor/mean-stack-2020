import { Post } from './post.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { compileDirectiveFromMetadata } from '@angular/compiler';



@Injectable({providedIn:'root'})
export class postsService {
private posts: Post[] = [];
private postsUpdatedSubjectObserver = new Subject<Post[]>();//observer

  constructor(private http: HttpClient, private router: Router){}

getPosts() {
//return [...this.posts];// copy array
  //return this.posts;
  debugger;
  this.http
    .get<{message: string, posts:any}>('http://localhost:3000/api/posts')
    .pipe(map( postData => {
        return postData.posts.map( post => {
          return {
            title: post.title,
            content: post.content,
            id: post._id,
            imagePath: post.imagePath
          };
        });
    }))
    .subscribe(transformedPosts => {
      debugger;
      this.posts = transformedPosts;
      this.postsUpdatedSubjectObserver.next([...this.posts]);
    });
}

getPost(id: string) {
  debugger;
  return this.http.get<{ _id: string, title: string, content: string}>('http://localhost:3000/api/posts/' + id);
}

getPostUpdateListener() {
  return this.postsUpdatedSubjectObserver.asObservable();
}

addPost(title: string, content: string, image: File) {
//const post: Post = { id: null, title: title, content: content};
const postData = new FormData();
postData.append("title", title);
postData.append("content", content);
postData.append("image", image, title );
this.http
  .post<{message: string, post: Post}>('http://localhost:3000/api/posts', postData)
  .subscribe((restData) => {
    console.log(restData.message);
    debugger;
    const post: Post = { id: restData.post.id, title: title, content: content, imagePath: restData.post.imagePath };
    this.posts.push(post);
    this.postsUpdatedSubjectObserver.next([...this.posts]);//observer active (subject)- not passive like clasic observer that we send in argument of observebel
    this.router.navigate(["/"]);
  });

}

updatePost(id: string,title: string, content: string){
  debugger;
  const post: Post = {id: id, title: title, content: content, imagePath: null};
  this.http.put('http://localhost:3000/api/posts/' + id, post)
  .subscribe( (response) => {
    debugger;
    console.log(response);
    const updatedPosts = [...this.posts];
    const oldPostIndex = updatedPosts.findIndex( p => p.id === post.id );
    updatedPosts[oldPostIndex] = post;
    this.posts = updatedPosts;
    this.postsUpdatedSubjectObserver.next([...this.posts]);
    this.router.navigate(["/"]);
  });
}

deletePost(postId: string) {
  debugger;
  this.http.delete('http://localhost:3000/api/posts/' + postId)
  .subscribe( () => {
    debugger;
    console.log('Delite');
    const updatePosts = this.posts.filter(post => post.id !== postId );
    this.posts = updatePosts;
    this.postsUpdatedSubjectObserver.next([...this.posts]);
  });
}
}
