import { Component, OnInit, OnDestroy /*Input*/ } from '@angular/core';
import { Post } from '../post.model';
import { Subscription } from 'rxjs';
import { postsService } from '../posts.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {

/*    posts = [
        {title:"first post", content:"first post content"},
        {title:"second post", content:"second post content"},
        {title:"Third post", content:"Third post content"}
];*/

//@Input() posts: Post[] = [];
posts: Post[] = [];
private postsSub: Subscription;
isLoading = false;
constructor( public postsService: postsService ) {}

/*postService: postService;
constructor(postService: postService ) {
	this.postService = postService;
	}*/

ngOnInit() {
  this.isLoading = true;
		//why i need this?
		//this.posts = this.postsService.getPosts();
    //observeble get observer, what return ??
debugger;
this.postsService.getPosts();

this.postsSub =	this.postsService.getPostUpdateListener()
.subscribe( (posts: Post[]) => {
  debugger;
  this.isLoading = false;
  this.posts = posts;
  });
}

onDelete(postId: string) {
  debugger;
  this.postsService.deletePost(postId);
}


ngOnDestroy() {
this.postsSub.unsubscribe();
}
}
