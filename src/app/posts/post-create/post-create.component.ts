import { Component, OnInit, /*EventEmitter, Output*/ } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
// import { Post } from '../post.model';

import { postsService } from '../posts.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Post } from '../post.model';
import { mimeType } from './mime-type.validator';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {

    enteredTitle = '';
    enteredContent = '';
	// @Output() postCreated = new EventEmitter<Post>();
    private mode = 'create';
    private postId: string;
    post: Post;
    isLoading = false;
    form: FormGroup;
    imagePreview: string;


  constructor(public postsService: postsService, public route: ActivatedRoute) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {
         validators: [Validators.required, Validators.minLength(3)]
        }),
        content:  new FormControl(null, {
          validators: [Validators.required]
         }),
        image:  new FormControl(null, {
           validators: [Validators.required],
           asyncValidators: [mimeType]
          })
    });
    this.route.paramMap.subscribe( ( params: any ) => {
      if (params.has('postId')) {
          debugger;
          this.mode = 'edit';
          this.postId = params.get('postId');
          this.isLoading = true;
          this.postsService.getPost(this.postId).subscribe(postData => {
            debugger;
            this.isLoading = false;
            this.post = {
              id: postData._id,
              title:postData.title,
              content: postData.content,
              imagePath:null
               };
              /*
               this.form.setValue({
                title: this.post.title,
                content: this.post.content
              })
              */
               this.form.patchValue({title: this.post.title});
               this.form.patchValue({content: this.post.content});
          });
      } else {
          debugger;
          this.mode = 'create';
          this.postId = null;

      }
    });
  }

  onSavePost() {
    debugger;
    //if(this.form.invalid){return;}
    this.isLoading = true;
    if(this.mode === 'create'){
      debugger;
      this.postsService.addPost(this.form.value.title, this.form.value.content, this.form.value.image);
    } else{
      debugger;
      this.postsService.updatePost(
        this.postId,
        this.form.value.title,
        this.form.value.content
        )
    }


    /*	const post: Post = {
      title:form.value.title,
      content:form.value.content};*/
     // this.postCreated.emit(post);// go to $event

    this.form.reset();
    }


    onImagePicked(event: Event) {
      debugger;
      const file = (event.target as HTMLInputElement).files[0];
      //like set value but only to one form control
      this.form.patchValue({image: file});
      debugger;
      //Triggers Causes activation of custom  validation
      this.form.get('image').updateValueAndValidity();


      const reader = new FileReader();
      reader.onload = () => {
        debugger;
        //Enter here as a resource loading ends
         //command to make the image into something that could be present
        this.imagePreview = reader.result as string;
      };
      debugger;
      //loading resource
      reader.readAsDataURL(file);
    }

}
