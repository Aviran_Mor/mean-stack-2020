const express = require("express");
// allows us to extract incoming files
const multer = require("multer");
const Post = require('../models/post');

const router = express.Router();
const MINE_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpeg',
  'image/jpg': 'jpg'
};

const storage = multer.diskStorage({
  //when multer try save file

  destination: (req, file, cb) => {
    const isValid = MINE_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mine type");
    if(isValid) {
      error = null;
    }
    //realetive to server.js file
    cb(null, "backend/images")
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLowerCase().split(' ').join('-');
    const ext = MINE_TYPE_MAP[file.mimetype];//extantion
    cb(null, name + '-' + Date.now() + '.' + ext);
  }
});

router.post("",multer({storage: storage}).single("image") ,(req, res, next) => {//run multer
  console.log("app.post");
  const url = req.protocol + '://' + req.get("host");
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: url + "/images" + req.file.filename
  });
  post.save().then(createdPost => {
    console.log(createdPost);
    res.status(201).json({
      message:'Post added succesfully!',
      post:{
        ...createdPost,
        imagePath: createdPost.imagePath
      }
    });
  } );
  console.log(post);

});

router.put("/:id", (req, res, next) =>{
  const post = new Post({
    _id: req.body.id,
    title: req.body.title,
    content: req.body.content
  })
  Post.updateOne({_id: req.params.id}, post).then(result => {
    console.log(result);
    res.status(200).json({message: 'Update succesful'});
  });

})


router.get("/:id", (req, res, next) =>{

  Post.findById(req.params.id).then(post => {

    if(post){
      res.status(200).json(post);
    }else{
      res.status(404).json({message: 'Post not found!'});
    }

    console.log(result);

  });

})




router.get("" ,(req, res, next) =>{
  /*
  const posts = [
    {
     id: 'asdasd',
     title:"First server-side post",
     content:"This is coming from the server"
    },
    {
      id: 'asdasd',
      title:"Second server-side post",
      content:"This is coming from the server"
     },
  ];
*/
console.log("app.get");
Post.find()
  .then(documents => {
    console.log(documents);
    res.status(200).json({
      message:'Posts fetched succesfully!',
      posts: documents
    });
  });


});


router.delete("/:id", (req, res, next) => {
  console.log(req.params.id);
  Post.deleteOne({_id: req.params.id}).then(result => {
    console.log(result);
    res.status(200).json({ message: "Post deleted!"});
  });

})


router.use((req, res, next) =>{
  res.send('Hello from express!');
});

module.exports = router;
